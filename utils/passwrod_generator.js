const bcrypt = require("bcrypt");

// // The hash Password Generator Function

async function hashPassword(original_password) {
  const hashedPass = await bcrypt.hash(original_password, 10);
  return hashedPass; //returns [object Promise]
}
module.exports = hashPassword;
