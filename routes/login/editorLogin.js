const express = require("express");
const router = express.Router();
const { generateEditorToken } = require("../../utils/Token_generator");
const editorModel = require("../../models/mongooseModels/editorModel");
const { check, validationResult } = require("express-validator");
const passChecker = require("../../utils/password_checker");

router.post(
  "/",
  [
    check("email", "Email length should be 10 to 30 characters")
      .isEmail()
      .isLength({ min: 10, max: 30 }),
    check("name", "Name length should be 10 to 20 characters").isLength({
      min: 10,
      max: 20,
    }),
    check("password", "Password length should be 8 to 10 characters").isLength({
      min: 8,
      max: 10,
    }),
  ],
  async (req, res) => {
    const result =
      validationResult(req).length > 0 ? validationResult(req) : false;
    if (!result) {
      const data = await editorModel.find({ email: req.body.email });
      if (data) {
        passChecker(req.body.password, data[0].password).then((result) => {
          if (result) {
            data[0].lastLoggedIn = Date.now()
            data[0].save()
            const token = generateEditorToken(req.body);
            res.send({
              message: "The editor logged in successfully \u{1F44D}",
              token,
              note: "To perform any action you have to add the token in your header and then make a request",
            });
          }else{
            res.send({
              message:"Your password is incorrect"
            })
          }
        });
      } else {
        res.send("no such user found please try signing up first");
      }
    } else {
      res.send(result);
    }
  }
);

module.exports = router;
