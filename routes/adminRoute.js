const express = require('express');
const router = express.Router()
const jwtVerify = require('../utils/Token_verifier')

router.use((req,res,next)=>{
    const result = jwtVerify.verifyAdminToken(req)
    if (!result){
        res.sendStatus(403)
        return
    }
    next()
})


router.get('/read',(req,res)=>{
        const head = req.headers
        res.send('ledger being ledger')
})

module.exports = router