const express = require('express');
const router = express.Router();
const {generateUserToken} = require('../../utils/Token_generator');
const userModel = require('../../models/mongooseModels/userModel')
const { check, validationResult } = require('express-validator');
const passGenerator = require('../../utils/passwrod_generator')

router.post('/',
[
    check('email', 'Email length should be 10 to 30 characters')
                    .isEmail().isLength({ min: 10, max: 30 }),
    check('name', 'Name length should be 10 to 20 characters')
                    .isLength({ min: 10, max: 20 }),
    check('password', 'Password length should be 8 to 10 characters')
                    .isLength({ min: 8, max: 10 })
],async (req,res)=>{
    const userData = req.body
    const result = validationResult(req).length > 0 ? validationResult(req) : false;
    const checks = await userModel.find({email : req.body.email})
        if (checks){
            res.send({
                message:"You already have an account please try signing in \u{1F625}"
            })
        }
    if(!result){

        const token = generateUserToken(userData);
        const dataToSave = new userModel(userData)
        dataToSave.password = passGenerator(dataToSave.password);
        await dataToSave.save()
        res.send({
            heading : "User created \u{1F44D}",
            message : "You successfully signedup \u{1F607}",
            token,
            note : "to send any type of request plese attach this token to your request's headers"
        })
    }
    else{
        res.send(result)
    }
})

module.exports = router
