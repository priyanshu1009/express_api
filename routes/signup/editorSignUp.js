const express = require('express');
const router = express.Router();
const {generateEditorToken} = require('../../utils/Token_generator');
const editorModel = require('../../models/mongooseModels/editorModel')
const { check, validationResult} = require('express-validator');
const hashPassword = require("../../utils/passwrod_generator");

router.post('/',
[
    check('email', 'Email length should be 10 to 30 characters')
                    .isEmail().isLength({ min: 10, max: 30 }),
    check('name', 'Name length should be 10 to 20 characters')
                    .isLength({ min: 10, max: 20 }),
    check('password', 'Password length should be 8 to 10 characters')
                    .isLength({ min: 8, max: 10 })
],async (req,res)=>{
    const userData = req.body
    const result = validationResult(req).length > 0 ? validationResult(req) : false;
    if(!result){
        const token = generateEditorToken(userData);
        const checks = await editorModel.find({email :userData.email});
        if (checks.length>0){
            res.send({
                message:"You already have an account please try signing in \u{1F625}"
            })
            return
        }else{
            hashPassword(userData.password).then((pass)=>{
                userData.password = pass;
                let dataToSave = new editorModel(userData);
                dataToSave.save().then(
                    ()=>{
                        res.send({
                            heading : "Editor created \u{1F44D}",
                            message:"You successfully signedup \u{1F607}",
                            token,
                            note:"to send any type of request plese attach this token to your request's headers"
                        })
                    }
                ).catch((err)=>{
                    res.send(err)
                })
            }).catch((err)=>{
                res.send(err)
            })
        }
    }
    else{
        res.send(result)
    }
})

module.exports = router
